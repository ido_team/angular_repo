import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
     li { cursor: pointer; }
     li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #2f70d8;
         border-color: #ecf0f1; 
         color: #2c3e50;
        }     
  `]
})
export class UsersComponent implements OnInit {

users;

currentUser;

constructor(private _usersService: UsersService) { }

  ngOnInit() {
     this._usersService.getUsers().subscribe(users => this.users = users);
     this.currentUser = -1;
   
  }

select(user){
  if(this.currentUser == -1){
  this.currentUser = user;
 
  }else{
    this.currentUser = -1;
  }
  console.log(user.id);

}

}
