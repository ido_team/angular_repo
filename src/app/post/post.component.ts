import { Component, OnInit } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styles:[`
    .posts{border:1px solid; margin-top:5px;}
  `],
  inputs:['post']
})
export class PostComponent implements OnInit {

post: Post;

  constructor() { }

  ngOnInit() { 
  }

}
